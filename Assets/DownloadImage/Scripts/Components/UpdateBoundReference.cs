﻿using System;
using UnityEngine;

namespace DownloadImage.Scripts.Components
{
    public class UpdateBoundReference : MonoBehaviour
    {
        public Action OnUpdate;
        
        private void Update()
        {
            OnUpdate?.Invoke();
        }
    }
}