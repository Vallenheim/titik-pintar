using DownloadImage.Scripts.Interfaces;
using DownloadImage.Scripts.Singletons;
using UnityEngine;
using UnityEngine.UI;

namespace DownloadImage.Scripts.Controllers
{
    public class WebImage : MonoBehaviour, IDownloadImage
    {
        [SerializeField] private string _url;
        [SerializeField] private Image _image;
        [SerializeField] private bool _cacheImage;

        private void Start()
        {
            DownloadImage();
        }
        
        public string Url => _url;
        public bool CacheImage => _cacheImage;
        public float SecondsWaiting { get; set; }

        private void DownloadImage()
        {
            QueueManager.Instance.QueueDownload(this);
        }

        public void HandleDownloadFinished(Texture2D texture)
        {
            var textureRect = new Rect(0, 0, texture.width, texture.height);
            _image.sprite = Sprite.Create(texture, textureRect, Vector2.zero);
        }
    }
}