using UnityEngine;

namespace DownloadImage.Scripts.Interfaces
{
    public interface IDownloadImage
    {
        string Url { get; }
        bool CacheImage { get; }
        float SecondsWaiting { get; set; }
        void HandleDownloadFinished(Texture2D texture);
    }
}
