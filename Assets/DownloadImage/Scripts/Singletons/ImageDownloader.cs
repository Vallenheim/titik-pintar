using System;
using System.Collections;
using System.IO;
using DownloadImage.Scripts.Extensions;
using DownloadImage.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Networking;

namespace DownloadImage.Scripts.Singletons
{
    public class ImageDownloader : Singleton<ImageDownloader>
    {
        private const int DaysToKeepImageCache = 7;

        public void StartDownload(IDownloadImage downloadImage, Action<IDownloadImage, Texture2D> handleDownloadComplete)
        {
            MonoBehaviour.StartCoroutine(GetTexture(downloadImage, handleDownloadComplete));
        }

        private IEnumerator GetTexture(IDownloadImage downloadImage, Action<IDownloadImage, Texture2D> handleDownloadComplete)
        {
            if (downloadImage.CacheImage && CheckForCache(downloadImage.Url))
            {
                yield return MonoBehaviour.StartCoroutine(LoadFromCache(downloadImage, handleDownloadComplete));
                
            }
            else
            {
                yield return MonoBehaviour.StartCoroutine(DownloadFromWeb(downloadImage, handleDownloadComplete));
            }
        }

        private IEnumerator LoadFromCache(IDownloadImage downloadImage, Action<IDownloadImage, Texture2D> handleDownloadComplete)
        {
            var texture = new Texture2D(2, 2);
            texture.LoadImage(System.IO.File.ReadAllBytes(GetFilePath(downloadImage.Url)));
            handleDownloadComplete(downloadImage, texture);
            
            yield break;
        }

        private IEnumerator DownloadFromWeb(IDownloadImage downloadImage,
            Action<IDownloadImage, Texture2D> handleDownloadComplete)
        {
            var www = UnityWebRequestTexture.GetTexture(downloadImage.Url);
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                var texture = ((DownloadHandlerTexture) www.downloadHandler).texture;
                if (downloadImage.CacheImage)
                {
                    CreateNewCache(downloadImage.Url, texture);
                }
                handleDownloadComplete(downloadImage, texture);
            }
        }

        private bool CheckForCache(string url)
        {
            var filePath = GetFilePath(url);
            var lastWriteTime = File.GetLastWriteTime(filePath);
            var timeSpan = DateTime.Now - lastWriteTime;
            return File.Exists(filePath) && timeSpan.Days < DaysToKeepImageCache;
        }

        private static string GetFilePath(string url)
        {
            var hashedUrl = SystemExtensions.GetHashString(url);
            return $"{Application.persistentDataPath}/{hashedUrl}.png";
        }

        private static void CreateNewCache(string url, Texture2D texture)
        {
            var png = texture.EncodeToPNG();
            File.WriteAllBytes(GetFilePath(url), png);
        }
    }
}