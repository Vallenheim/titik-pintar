using System.Collections.Generic;
using System.Linq;
using DownloadImage.Scripts.Components;
using DownloadImage.Scripts.Interfaces;
using UnityEngine;

namespace DownloadImage.Scripts.Singletons
{
    public class QueueManager : Singleton<QueueManager>
    {
        private const float SecondsWaitingThreshold = 10f;

        private readonly List<IDownloadImage> _downloadsQueued = new List<IDownloadImage>();
        private readonly List<IDownloadImage> _downloadsInProgress = new List<IDownloadImage>();

        protected override void ConstructSingleton()
        {
            var updateReference = new GameObject().AddComponent<UpdateBoundReference>();
            updateReference.OnUpdate += Update;
        }

        public void QueueDownload(IDownloadImage downloadImage)
        {
            _downloadsQueued.Add(downloadImage);
        }

        private void Update()
        {
            QueueNewDownloadJob();

            UpdateQueueWaitingDuration();

            CheckDelayedJobs();
        }

        private void QueueNewDownloadJob()
        {
            if (_downloadsQueued.Count > 0 && _downloadsInProgress.Count < 3)
            {
                var downloadImage = _downloadsQueued[0];
                StartDownload(downloadImage);
            }
        }

        private void UpdateQueueWaitingDuration()
        {
            foreach (var downloadImage in _downloadsQueued)
            {
                downloadImage.SecondsWaiting += Time.deltaTime;
            }
        }

        private void CheckDelayedJobs()
        {
            var isAnyJobsPassesThreshold = _downloadsQueued.Any(j => j.SecondsWaiting >= SecondsWaitingThreshold);
            while (isAnyJobsPassesThreshold)
            {
                var downloadJob = _downloadsQueued.First(j => j.SecondsWaiting >= SecondsWaitingThreshold);
                StartDownload(downloadJob);
                isAnyJobsPassesThreshold = _downloadsQueued.Any(j => j.SecondsWaiting >= SecondsWaitingThreshold);
            }
        }

        private void StartDownload(IDownloadImage downloadImage)
        {
            _downloadsQueued.Remove(downloadImage);
            _downloadsInProgress.Add(downloadImage);

            ImageDownloader.Instance.StartDownload(downloadImage, HandleDownloadComplete);
        }
        
        private void HandleDownloadComplete(IDownloadImage downloadImage, Texture2D texture)
        {
            _downloadsInProgress.Remove(downloadImage);
            downloadImage.HandleDownloadFinished(texture);
        }
    }
}