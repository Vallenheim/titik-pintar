﻿using DownloadImage.Scripts.Components;
using UnityEngine;

namespace DownloadImage.Scripts.Singletons
{
    public class Singleton<T> where T : Singleton<T>, new()
    {
        protected MonoBehaviour MonoBehaviour;

        protected Singleton()
        {
            
        }

        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                    _instance.ConstructSingleton();
                }

                return _instance;
            }
        }
        
        protected virtual void ConstructSingleton()
        {
            MonoBehaviour = new GameObject().AddComponent<MonobehaviourReference>();
        }
    }
}