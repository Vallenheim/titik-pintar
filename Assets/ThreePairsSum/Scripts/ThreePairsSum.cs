using System.Linq;
using UnityEngine;

namespace ThreePairsSum.Scripts
{
    public class ThreePairsSum : MonoBehaviour
    {
        public static bool Pairs2Sum(int[] numbers, int sum = 0)
        {
            foreach (var number in numbers)
            {
                if (numbers.Contains((sum - number)))
                {
                    return true;
                }
            }

            return false;
        }
    
        public static bool Pairs3Sum(int[] numbers, int sum = 0)
        {
            foreach (var number in numbers)
            {
                var temporarySum = sum - number;
                var clone = numbers.Where(n => n != number).ToArray();
                if (Pairs2Sum(clone, temporarySum))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
