using NUnit.Framework;

namespace ThreePairsSum.Tests
{
    public class ValidationTests
    {
        [Test]
        [TestCase(new[] {-7, -5, 4, 5, 6}, true)]
        [TestCase(new[] {-7, -3, 4, 6, 10, 15}, false)]
        public void Pairs2Sum_ArrayOfNumbers_ExpectedResult(int[] numbers, bool expectedResult)
        {
            var result = global::ThreePairsSum.Scripts.ThreePairsSum.Pairs2Sum(numbers, 0);
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        [TestCase(new[] {-7, -5, 4, 5, 6}, false)]
        [TestCase(new[] {-7, -3, 4, 6, 10, 15}, true)]
        public void Pairs3Sum_ArrayOfNumbers_ExpectedResult(int[] numbers, bool expectedResult)
        {
            var result = global::ThreePairsSum.Scripts.ThreePairsSum.Pairs3Sum(numbers, 0);
            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}