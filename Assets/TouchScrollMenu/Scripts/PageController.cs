using UnityEngine;

namespace TouchScrollMenu.Scripts
{
    public class PageController : MonoBehaviour
    {
        private static PageController ActivePage;

        private void Start()
        {
            if (gameObject.activeSelf)
            {
                ActivePage = this;
            }
        }
        
        public void ShowPage()
        {
            ActivePage?.Hide();

            ActivePage = this;
            ActivePage.Show();
        }

        private void Show()
        {
            gameObject.SetActive(true);
        }
        
        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
