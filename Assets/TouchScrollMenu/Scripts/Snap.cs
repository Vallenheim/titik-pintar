using UnityEngine;
using UnityEngine.UI;

namespace TouchScrollMenu.Scripts
{
    public class Snap : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private RectTransform _content;

        private void Start()
        {
            if (_scrollRect == null)
            {
                _scrollRect = GetComponent<ScrollRect>();
            }

            _content = _scrollRect.content;
        }

        private void Update()
        {
            if (_scrollRect.velocity == Vector2.zero)
            {
                var scrollPosition = _content.anchoredPosition;
                var xPosition = scrollPosition.x;
                var roundedPosition = Mathf.RoundToInt(xPosition / 600f);
                scrollPosition.x = roundedPosition * 600f;
                _content.anchoredPosition = Vector2.Lerp(_content.anchoredPosition, scrollPosition, 8 * Time.deltaTime);
            }
        }
    }
}